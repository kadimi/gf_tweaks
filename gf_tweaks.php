<?php
/*
Plugin Name: Gravity Form Tweaks
Plugin URI: https://app.codeable.io/tasks/30388/workroom
Description: Gravity Form Tweaks
Version: 1.0.0
Author: Nabil Kadimi
Author URI: http://kadimi.com
License: GPL2
*/

/**
 * Plugin configuration.
 */
$gf_tweaks_conf = array(
	'affected_forms_ids' => array(
		2,
		'Add more form IDs here',
		'Add more form IDs here',
		'Add more form IDs here',
	),
	'affected_notifications_ids' => array(
		'568b6658c5525',
		'Add more notification IDs here',
		'Add more notification IDs here',
		'Add more notification IDs here',
	),
);


/**
 * Don't edit past this line.
 */

/**
 * Add attachements notifications
 *
 * This code does't use the deprecated hook `gform_user_notification_attachments`
 * as in this example from Gravity Forms API documentation:
 * https://www.gravityhelp.com/documentation/article/gform_notification/#5-attach-single-file-uploads
 *
 * This code is applied to forms notifications whose:
 *   - id is in the configuration option `$gf_tweaks_conf[ 'affected_notifications_ids' ]`.
 *   - parent form id is in the configuration option `$gf_tweaks_conf[ 'affected_forms_ids' ]`.
 */
add_filter( 'gform_notification', 'my_gform_notification_signature', 10, 3 );
function my_gform_notification_signature( $notification, $form, $entry ) {

	global $gf_tweaks_conf;

	if ( ! in_array( $form[ 'id' ], $gf_tweaks_conf[ 'affected_forms_ids' ] ) ) {
		return $notification;
	}

	if ( ! in_array( $notification[ 'id' ], $gf_tweaks_conf[ 'affected_notifications_ids' ] ) ) {
		return $notification;
	}

	$field_id = gf_tweaks_get_1st_checkboxes_id( $form );
	$attachments_urls = array();
	$attachments_paths = array();

	for ( $i = 1; $i < 100; $i++ ) {
		$v = $entry[ "$field_id.$i" ];
		if ( $v ) {
			$attachments_urls[] = $v;
		}
	}

	$upload_dir = wp_upload_dir();
	foreach ( $attachments_urls as $url ) {
		$attachments_paths[] = str_replace(
			$upload_dir[ 'baseurl' ],
			$upload_dir[ 'basedir' ],
			$url
		);
	}

	$notification[ 'attachments' ] = $attachments_paths;
	return $notification;
}

/**
 * Get the field id of the first checkboxes field in the provided form.
 */
function gf_tweaks_get_1st_checkboxes_id( $form ) {
	foreach ( $form[ 'fields' ] as $field ) {
		if ( is_a( $field, 'GF_Field_Checkbox' ) ) {
			return $field->id;
		}
	}
	return false;
}
